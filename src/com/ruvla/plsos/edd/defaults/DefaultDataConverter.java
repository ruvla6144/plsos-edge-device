package com.ruvla.plsos.edd.defaults;

import com.ruvla.plsos.edd.framework.ConfirmationPacket;
import com.ruvla.plsos.edd.framework.DataConverter;

public abstract class DefaultDataConverter implements DataConverter {

	@Override
	public ConfirmationPacket convertBytesToConfirmationPacket(byte[] bytes) {
		ConfirmationPacket packet = new ConfirmationPacket();
		if (packet.equals(String.valueOf(bytes)))
			return packet;
		else return null;
	}

}
