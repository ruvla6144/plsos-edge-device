package com.ruvla.plsos.edd.defaults;

import java.util.Queue;

import com.ruvla.plsos.edd.framework.CommDeviceDataMessage;
import com.ruvla.plsos.edd.framework.ConfirmationPacket;
import com.ruvla.plsos.edd.framework.Core;
import com.ruvla.plsos.edd.framework.DataBatch;
import com.ruvla.plsos.edd.framework.DataConverter;
import com.ruvla.plsos.edd.framework.LogMessage;
import com.ruvla.plsos.edd.framework.PLSOSConnector;
import com.ruvla.plsos.edd.framework.PLSOSException;
import com.ruvla.plsos.edd.framework.PLSOSMessage;
import com.ruvla.plsos.edd.framework.PLSOSMessageSubscriber;
import com.ruvla.plsos.edd.framework.PLSOSMessageSystem;
import com.ruvla.plsos.edd.framework.TransferSession;

public class DefaultTransferSession extends TransferSession implements PLSOSMessageSubscriber {

	private volatile boolean stop;
	private DataConverter dataConverter = Core.getFactory().createDataConverter();
	private ConfirmationPacket confirmationPacket;
	private Object confirmationPacketWaiter = new Object();
	private volatile boolean confirmationReceived = false;
	
	public DefaultTransferSession(PLSOSConnector connector, Queue<DataBatch> batches) {
		super(connector, batches);
		stop = false;
		//PLSOSMessageSystem.subscribe(this, connector.getCommDevice());
	}

	@Override
	public void run() {
		connector.getCommDevice().connect(connector);;
		while (!stop) {
			DataBatch batch;
			confirmationReceived = false;
			try {
				batch = dequeue();
				connector.getCommDevice().send(connector, dataConverter.convertDataBatchToBytes(batch));
				/*waitConfirmation();
				if (confirmationPacket != null)
					PLSOSMessageSystem.publish(new LogMessage(this, confirmationPacket.toString()));
				else PLSOSMessageSystem.publish(new LogMessage(this, "Wrong Confirmation Packet"));*/
					
			} catch (InterruptedException e) {
				connector.getCommDevice().disconnect(connector);
				PLSOSMessageSystem.publish(new LogMessage(this, "TransferSession has been interrupted"));
			}
		}
		connector.getCommDevice().disconnect(connector);
	}

	@Override
	public void stop() {
		stop = true;
	}

	@Override
	public void handleMessage(PLSOSMessage message) throws PLSOSException {
		ConfirmationPacket confirmationPacket = dataConverter.convertBytesToConfirmationPacket(((CommDeviceDataMessage)message).data);
		synchronized (confirmationPacketWaiter) {
			this.confirmationPacket = confirmationPacket;
			confirmationReceived = true;
			confirmationPacketWaiter.notifyAll();
		}
	}
	
	protected void waitConfirmation() throws InterruptedException {
		synchronized (confirmationPacketWaiter) {
			while (!confirmationReceived) {
				confirmationPacketWaiter.wait();				
			}
		}
	}
}
