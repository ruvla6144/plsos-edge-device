package com.ruvla.plsos.edd.defaults;

import com.ruvla.plsos.edd.framework.Logger;
import com.ruvla.plsos.edd.framework.PLSOSException;
import com.ruvla.plsos.edd.framework.PLSOSMessage;
import com.ruvla.plsos.edd.framework.PLSOSMessageSystem;
import com.ruvla.plsos.edd.framework.PLSOSMessage.Subject;

public class DefaultLogger implements Logger {
	
	{PLSOSMessageSystem.subscribe(this, Subject.ANY);}
	
	public void log(String str){
		System.out.println(str.replace('\n', ' ').replace('\r', ' '));
	}

	@Override
	public void handleMessage(PLSOSMessage message) throws PLSOSException {
		log(message.toString());
		
	}
}