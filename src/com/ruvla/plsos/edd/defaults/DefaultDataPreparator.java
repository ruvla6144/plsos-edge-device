package com.ruvla.plsos.edd.defaults;

import com.ruvla.plsos.edd.framework.DataPreparator;

public class DefaultDataPreparator extends DataPreparator {

	@Override
	public void prepareData() {
		while (!data.isEmpty())
			publishPreparedData(data.poll());
	}

}
