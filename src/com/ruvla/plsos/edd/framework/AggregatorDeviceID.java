package com.ruvla.plsos.edd.framework;

public class AggregatorDeviceID {
	
	private long id;
	
	public AggregatorDeviceID(long id){
		this.id = id;
	}
	
	@Override
	public String toString() {
		return String.valueOf(id);
	}
	
	public byte[] toBytes() {
		return toString().getBytes();
	}
	
	@Override
	public int hashCode() {
		return (id + getClass().getName()).hashCode();
	}

	@Override
	public boolean equals(Object obj) {
		if (obj == null)
			return false;
		if (obj.getClass() == long.class) return id == (long)obj;
		if (this == obj)
			return true;
		if (getClass() != obj.getClass())
			return false;
		AggregatorDeviceID other = (AggregatorDeviceID) obj;
		return id == other.id;
	}
}
