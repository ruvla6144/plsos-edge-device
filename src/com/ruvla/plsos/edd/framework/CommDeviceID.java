package com.ruvla.plsos.edd.framework;

public class CommDeviceID {
	@Override
	public String toString() {
		return id + "(" + type + ")";
	}

	private int id;
	private String type;
	
	public CommDeviceID(int id, String type){
		this.id = id;
		this.type = type;
	}
	
	@Override
	public int hashCode() {
		return (id + getClass().getName()).hashCode();
	}

	@Override
	public boolean equals(Object obj) {
		if (obj == null)
			return false;
		if (obj.getClass() == int.class) return id == (int)obj;
		if (this == obj)
			return true;
		if (getClass() != obj.getClass())
			return false;
		CommDeviceID other = (CommDeviceID) obj;
		if (id != other.id || type != other.type)
			return false;
		return true;
	}
}
