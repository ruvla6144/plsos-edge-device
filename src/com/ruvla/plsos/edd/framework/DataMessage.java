package com.ruvla.plsos.edd.framework;

public class DataMessage extends PLSOSMessage {

	public final Data data;

	public DataMessage(Object publisher, Data data) {
		super(publisher, Subject.DATA);
		this.data = data;
	}
	
	public DataMessage(DataPreparator publisher, Data data) {
		super(publisher, Subject.DATA_FROM_PREPARATOR);
		this.data = data;
	}
	
	public <T extends Data> DataMessage(SensorJob<T> publisher, T data) {
		super(publisher, Subject.DATA_FROM_JOB);
		this.data = data;
	}
	
	public <T extends Data> DataMessage(Sensor<T> publisher, T data) {
		super(publisher, Subject.DATA_FROM_SENSOR);
		this.data = data;
	}
	
	@Override
	public String toString(){
		return super.toString() + "Data = " + data.toString() + " ";
	}
}
