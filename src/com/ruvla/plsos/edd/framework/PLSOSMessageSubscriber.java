package com.ruvla.plsos.edd.framework;

public interface PLSOSMessageSubscriber {
	public void handleMessage(PLSOSMessage message) throws PLSOSException;
}
