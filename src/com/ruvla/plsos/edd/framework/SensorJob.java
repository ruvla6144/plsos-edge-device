package com.ruvla.plsos.edd.framework;

import com.ruvla.plsos.edd.framework.PLSOSMessage.Subject;

public abstract class SensorJob<T extends Data> implements Runnable, PLSOSMessageSubscriber {
	
	public final JobID id;
	
	public SensorJob(JobID id){
		this.id = id;
	}

	public abstract void execute() throws InterruptedException, PLSOSException;
	
	@Override
	public void run() { //Should be synchronized?
		try{
			execute();
		}
		catch (InterruptedException | PLSOSException exc){
			finish();
			PLSOSMessageSystem.publish(new PLSOSMessage(this, Subject.JOB_STOPPED_EVENT));
		}
	}
	
	public void handleMessage(PLSOSMessage message) throws PLSOSException {
		if (message.subject == Subject.DATA_FROM_SENSOR) 
			handleSensorData((Sensor<?>) message.publisher, (DataMessage) message);
	}
	
	public void publishJobData(T jobData) {
		PLSOSMessageSystem.publish(new DataMessage(this, jobData));
	}
	
	public abstract void handleSensorData(Sensor<?> sensor, DataMessage sensorData) throws PLSOSException;
	
	protected abstract void finish();
	
	@Override
	public String toString(){
		return "SensorJob ID = " + id + " ";
	}
}
