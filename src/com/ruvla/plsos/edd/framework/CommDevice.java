package com.ruvla.plsos.edd.framework;

import com.ruvla.plsos.edd.framework.PLSOSMessage.Subject;

public abstract class CommDevice {
	
	public final CommDeviceID commDeviceID;
	
	public CommDevice(CommDeviceID commDeviceID) {
		this.commDeviceID = commDeviceID;
	}
	
	public void start() throws InterruptedException {
		startDevice();
		PLSOSMessageSystem.publish(this, Subject.COMMDEVICE_STARTED_EVENT);
	}
	
	public void stop() {
		stopDevice();
		PLSOSMessageSystem.publish(this, Subject.COMMDEVICE_STOPPED_EVENT);
	}
	
	public abstract void startDevice() throws InterruptedException;
	public abstract void stopDevice();
	public abstract void connect(PLSOSConnector connector);
	public abstract void disconnect(PLSOSConnector connector);
	public abstract void send(PLSOSConnector connector, byte[] data);
	public abstract byte[] receive(PLSOSConnector connector); //Is it correct for any type of comm device?
	
	protected void publishNewConnector(PLSOSConnector plsosConnector){
		PLSOSMessageSystem.publish(new NewConnectorMessage(this, plsosConnector));
	}
	
	protected void publishNewData(byte[] data) {
		PLSOSMessageSystem.publish(new CommDeviceDataMessage(this, data));
	}
	
	protected void onAggregatorLost() {
		PLSOSMessageSystem.publish(new PLSOSMessage(this, Subject.AGGREGATOR_DEVICE_DISCONNECTED_EVENT));
	}
	
	public String toString(){
		return "CommDevice ID = " + commDeviceID + " ";
	}
}
