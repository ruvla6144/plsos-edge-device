package com.ruvla.plsos.edd.framework;

import java.util.LinkedList;
import java.util.Queue;

import com.ruvla.plsos.edd.framework.PLSOSMessage.Subject;

public abstract class DataStorage implements PLSOSMessageSubscriber {
	
	protected Queue<Data> preparedData = new LinkedList<Data>();
	
	{
		PLSOSMessageSystem.subscribe(this, Subject.SHUTDOWN_PLSOS);
		PLSOSMessageSystem.subscribe(this, Subject.DATA_FROM_PREPARATOR);
		PLSOSMessageSystem.subscribe(this, Subject.START_DATA_STORING);
		PLSOSMessageSystem.subscribe(this, Subject.RETRIEVE_DATA_FROM_STORAGE);
	}
	
	public abstract void storeData();
	public abstract Data retrieveData();
	public abstract DataBatch retrieveDataBatch();
	public abstract boolean hasData();

	@Override
	public void handleMessage(PLSOSMessage message) throws PLSOSException {
		if (message.subject == Subject.DATA_FROM_PREPARATOR) {
			synchronized (preparedData) {
				preparedData.add(((DataMessage)message).data); 
			}
			return;
		} else if (message.subject == Subject.START_DATA_STORING) {
			storeData();
			PLSOSMessageSystem.publish(new PLSOSMessage(this, Subject.DATA_STORED_EVENT));
			return;
		} else if (message.subject == Subject.RETRIEVE_DATA_FROM_STORAGE) {
			while (hasData()) PLSOSMessageSystem.publish(new DataBatchMessage(this, retrieveDataBatch()));
		} else if (message.subject == Subject.SHUTDOWN_PLSOS) {
			shutdown();
			return;
		}
	}
	
	@Override
	public String toString(){
		return "DataStorage" + " ";
	}
	
	protected abstract void shutdown();
}
