package com.ruvla.plsos.edd.framework;

public class CommDeviceDataMessage extends PLSOSMessage{
	
	public final byte[] data;

	public CommDeviceDataMessage(CommDevice publisher, byte[] data) {
		super(publisher, Subject.COMMDEVICE_DATA);
		this.data = data;
	}
	
	

}
