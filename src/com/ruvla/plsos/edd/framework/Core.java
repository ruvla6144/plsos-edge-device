package com.ruvla.plsos.edd.framework;

import com.ruvla.plsos.edd.framework.PLSOSMessage.Subject;

public class Core {
	
	public static final long ID = 256;
	
	private static AbstractFactory factory = null;
	private Logger logger;
	private SensorJobExecutor sensorJobExecutor;
	private CommDeviceManager detectionManager;
	private Scheduler scheduler;
	private DataPreparator dataPreparator;
	private DataStorage dataStorage;
	private TransferManager transferManager;
	
	public Core(AbstractFactory factory) throws PLSOSException{
		if (factory == null)
			throw new PLSOSException("Factory cannot be NULL");
		Core.factory = factory;
		logger = factory.createLogger();
		logger.log("PLSOS SYSTEM IS STARTING!");
		sensorJobExecutor = factory.createSensorJobExecutor();
		dataPreparator = factory.createDataPreparator();
		dataStorage = factory.createDataStorage();
		detectionManager = factory.createCommDeviceManager();
		transferManager = factory.createTransferManager();
		scheduler = factory.createScheduler();
	}
	
	public void shutdown(){
		PLSOSMessageSystem.publish(new PLSOSMessage(this, Subject.SHUTDOWN_PLSOS));
		logger.log("PLSOS SYSTEM IS SHUTTING DOWN!");
	}
	
	public static AbstractFactory getFactory(){
		return Core.factory;
	}
}
