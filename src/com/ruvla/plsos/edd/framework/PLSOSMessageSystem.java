package com.ruvla.plsos.edd.framework;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import com.ruvla.plsos.edd.framework.PLSOSMessage.Subject;

public class PLSOSMessageSystem {
	private static Map<Subject, Set<PLSOSMessageSubscriber>> subject_subscribers = new HashMap<Subject, Set<PLSOSMessageSubscriber>>();
	private static Map<Object, Set<PLSOSMessageSubscriber>> instance_subscribers = new HashMap<Object, Set<PLSOSMessageSubscriber>>();
	
	static {
		for (Subject subject : Subject.values())
		{
			subject_subscribers.put(subject, new HashSet<PLSOSMessageSubscriber>());
		}
	}
	
	public static void publish(PLSOSMessage message) {
		if (message.subject == null) return;
		for (Subject cur_subject : subject_subscribers.keySet())
		{
			if (message.subject.is(cur_subject)){
				for (PLSOSMessageSubscriber subscriber : subject_subscribers.get(cur_subject)) {
					try {
						subscriber.handleMessage(message);
					} catch (PLSOSException e) {
						e.printStackTrace(); //TODO
					}
				}
			}
		}
		if (instance_subscribers.containsKey(message.publisher))
		for (PLSOSMessageSubscriber subscriber : instance_subscribers.get(message.publisher))
		{
			try {
				subscriber.handleMessage(message);
			} catch (PLSOSException e) {
				e.printStackTrace(); //TODO
			}
		}
	}
	
	public static void publish(Object publisher, Subject subject) {
		publish(new PLSOSMessage(publisher, subject));
	}
	
	public synchronized static void subscribe(PLSOSMessageSubscriber subscriber, Subject subject){
		subject_subscribers.get(subject).add(subscriber);
	}
	
	public synchronized static void subscribe(PLSOSMessageSubscriber subscriber, Object instance){
		if (!instance_subscribers.containsKey(instance)) instance_subscribers.put(instance, new HashSet<PLSOSMessageSubscriber>());
		instance_subscribers.get(instance).add(subscriber);
	}
	
	public synchronized static void unsubscribe(PLSOSMessageSubscriber subscriber, Subject subject){
		subject_subscribers.get(subject).remove(subscriber);
	}
}