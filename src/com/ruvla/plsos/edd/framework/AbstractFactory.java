package com.ruvla.plsos.edd.framework;

import java.util.List;
import java.util.Queue;

public abstract class AbstractFactory {
	
	public abstract Logger createLogger();
	
	public abstract SensorJobExecutor createSensorJobExecutor();
	
	public abstract Scheduler createScheduler();
	
	public abstract CommDeviceManager createCommDeviceManager();
	
	public abstract TransferManager createTransferManager();
	
	//public abstract PreparedData createPreparedDataFromByteArray(byte[] bytes);
	
	//public abstract List<Sensor<?>> createSensors();
	
	public abstract DataConverter createDataConverter();
	
	public abstract Sensor<?> createSensor(SensorID id);
	
	public abstract List<JobID> createJobIDList();
	
	public abstract SensorJob<?> createSensorJob(JobID id);
	
	public abstract DataPreparator createDataPreparator();
	
	public abstract DataStorage createDataStorage();
	
	public abstract List<CommDeviceID> createCommDeviceIDList();
	
	public abstract CommDevice createCommDevice(CommDeviceID commDeviceID);
	
	public abstract TransferSession createTransferSession(PLSOSConnector connector, Queue<DataBatch> batches);
}