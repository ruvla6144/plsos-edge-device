package com.ruvla.plsos.edd.framework;

public class PLSOSConnector {
	
	public final CommDeviceID commDeviceID;
	public final AggregatorDeviceID aggregatorDeviceID;
	protected CommDevice commDevice;
	
	public PLSOSConnector(CommDeviceID commDeviceID, AggregatorDeviceID aggregatorDeviceID) {
		this.commDeviceID = commDeviceID;
		this.aggregatorDeviceID = aggregatorDeviceID;
	}
	
	/*public AggregatorDeviceID getAggregatorDeviceID() {
		return aggregatorDeviceID;
	}

	public void setAggregatorDeviceID(AggregatorDeviceID aggregatorDeviceID) {
		this.aggregatorDeviceID = aggregatorDeviceID;
	}*/

	public CommDevice getCommDevice() {
		return commDevice;
	}

	public void setCommDevice(CommDevice commDevice) {
		this.commDevice = commDevice;
	}
	
}
