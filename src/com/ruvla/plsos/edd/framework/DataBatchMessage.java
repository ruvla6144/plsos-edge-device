package com.ruvla.plsos.edd.framework;

public class DataBatchMessage extends PLSOSMessage {

	public final DataBatch data;
	
	public DataBatchMessage(Object publisher, DataBatch data) {
		super(publisher, Subject.DATA_BATCH);
		this.data = data;
	}
	
	public DataBatchMessage(DataStorage publisher, DataBatch data) {
		super(publisher, Subject.DATA_FROM_STORAGE);
		this.data = data;
	}
	
}
