package com.ruvla.plsos.edd.framework;

import java.util.Queue;

public abstract class TransferSession implements Runnable {
	
	protected final PLSOSConnector connector;
	protected final Queue<DataBatch> batches;
	
	public TransferSession(PLSOSConnector connector, Queue<DataBatch> batches) {
		this.connector = connector;
		this.batches = batches;
	}
	
	public abstract void stop();
	
	protected DataBatch dequeue() throws InterruptedException{
		DataBatch batch;
		synchronized (batches) {
			while (batches.isEmpty()) {
				batches.wait();
			}
			batch = batches.poll();
		}
		return batch;
	}
}
