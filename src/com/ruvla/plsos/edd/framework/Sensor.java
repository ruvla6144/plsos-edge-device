package com.ruvla.plsos.edd.framework;

public abstract class Sensor<T extends Data>{
	public final SensorID id;
	
	public Sensor(SensorID id){
		this.id = id;
	}
	
	public abstract T read() throws InterruptedException; //Should be synchronized?
	
	public abstract void start() throws InterruptedException, PLSOSException;
	
	public abstract void stop() throws InterruptedException;
	
	protected void notifySubscribers(T data) throws PLSOSException{
		PLSOSMessageSystem.publish(new DataMessage(this, data));
	}
	
	@Override
	public String toString(){
		return "Sensor ID = " + id;
	}
}