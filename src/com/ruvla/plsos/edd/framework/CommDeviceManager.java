package com.ruvla.plsos.edd.framework;

import java.util.HashMap;
import java.util.Map;
import com.ruvla.plsos.edd.framework.CommDeviceActionMessage.CommDeviceAction;
import com.ruvla.plsos.edd.framework.PLSOSMessage.Subject;

public class CommDeviceManager implements PLSOSMessageSubscriber {
	
	protected final Map<CommDeviceID, CommDevice> commDevices = new HashMap<CommDeviceID, CommDevice>();
	protected final Map<CommDeviceID, Thread> threads = new HashMap<CommDeviceID, Thread>();
	
	{
		PLSOSMessageSystem.subscribe(this, Subject.SHUTDOWN_PLSOS);
		PLSOSMessageSystem.subscribe(this, Subject.COMMDEVICE_ACTION);
		PLSOSMessageSystem.subscribe(this, Subject.AGGREGATOR_DEVICE_DETECTED);
		PLSOSMessageSystem.subscribe(this, Subject.COMMDEVICE_NEW_CONNECTOR);
	}
	
	public CommDeviceManager() {
		
	}
	
	protected void shutdown() {
		stopAllCommDevices();
	}
	
	protected synchronized void startAllCommDevices() {
		for (CommDeviceID commDeviceID : commDevices.keySet()){
			try {
				startCommDevice(commDeviceID);
			} catch (PLSOSException e) {
				e.printStackTrace(); // TODO
			}
		}
	}
	
	protected synchronized void stopAllCommDevices() {
		for (CommDeviceID commDeviceID : commDevices.keySet()){
			try {
				stopCommDevice(commDeviceID);
			} catch (PLSOSException e) {
				e.printStackTrace(); // TODO
			}
		}
	}
	
	protected synchronized void startCommDevice(CommDeviceID commDeviceID) throws PLSOSException {
		CommDevice commDevice = Core.getFactory().createCommDevice(commDeviceID);
		Thread thread = new Thread(new Runnable() {
			
			@Override
			public void run() {
				try {
					commDevice.start();
				} catch (InterruptedException e) {
					PLSOSMessageSystem.publish(new LogMessage(this, "CommDevice Thread Interrupted"));
				}
				
			}
		});
		thread.start();
		commDevices.put(commDeviceID, commDevice);
		threads.put(commDeviceID, thread);
	}
	
	protected synchronized void stopCommDevice(CommDeviceID commDeviceID) throws PLSOSException{
		if (commDevices.containsKey(commDeviceID)) {
			CommDevice commDevice = commDevices.get(commDeviceID);
			Thread thread = threads.get(commDeviceID);
			thread.interrupt(); //TODO it is not enough to be sure that thread stopped
			commDevice.stop();
			commDevices.remove(commDeviceID);
			threads.remove(commDeviceID);
		}
		else 
			throw new PLSOSException("No such CommDeviceID in CommDeviceManager");
	}
	
	protected void prepareConnector(PLSOSConnector connector) {
		connector.setCommDevice(commDevices.get(connector.commDeviceID));
	}

	@Override
	public void handleMessage(PLSOSMessage message) throws PLSOSException {
		if (message instanceof CommDeviceActionMessage){
			handleMessage((CommDeviceActionMessage)message);
		} else if (message.subject == Subject.COMMDEVICE_NEW_CONNECTOR) {
			handleMessage((NewConnectorMessage)message);
		} else if (message.subject == Subject.SHUTDOWN_PLSOS){
			shutdown();
			return;
		}
	}
	
	public void handleMessage(NewConnectorMessage message) throws PLSOSException{
		prepareConnector(((NewConnectorMessage) message).plsosConnector);
		PLSOSMessageSystem.publish(new NewConnectorMessage(this, ((NewConnectorMessage) message).plsosConnector));
	}
	
	public void handleMessage(CommDeviceActionMessage message) throws PLSOSException{
		if (message.action == CommDeviceAction.START) startCommDevice(message.commDeviceID);
		else stopCommDevice(message.commDeviceID);
	}
}
