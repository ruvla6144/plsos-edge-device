package com.ruvla.plsos.edd.framework;

public class JobActionMessage extends PLSOSMessage {
	public enum JobAction {START, STOP}
	public final JobAction action;
	public final JobID jobID;
	
	public JobActionMessage(Object publisher, JobAction action, JobID jobID){
		super(publisher, Subject.SENSOR_JOB_ACTION);
		this.action = action;
		this.jobID = jobID;
	}
	
	@Override
	public String toString(){
		return super.toString() + "Action = " + action + " JobID = " + jobID + " ";
	}
}