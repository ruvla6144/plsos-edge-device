package com.ruvla.plsos.edd.framework;

public interface Logger extends PLSOSMessageSubscriber {

	public abstract void log(String str);

}
