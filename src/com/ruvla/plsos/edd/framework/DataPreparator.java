package com.ruvla.plsos.edd.framework;

import java.util.LinkedList;
import java.util.Queue;

import com.ruvla.plsos.edd.framework.PLSOSMessage.Subject;


public abstract class DataPreparator implements PLSOSMessageSubscriber {
	
	protected Queue<Data> data = new LinkedList<Data>();
	
	{
		PLSOSMessageSystem.subscribe(this, Subject.START_DATA_PREPARATION);
		PLSOSMessageSystem.subscribe(this, Subject.DATA_FROM_JOB);
	}
	
	public abstract void prepareData();
	
	
	public void handleMessage(PLSOSMessage message){
		if (message.subject == Subject.DATA_FROM_JOB) {
			synchronized (data) {
				data.add(((DataMessage) message).data);
			}
			return;
		}
		if (message.subject == Subject.START_DATA_PREPARATION) {
			prepareData();
			PLSOSMessageSystem.publish(new PLSOSMessage(this, Subject.DATA_PREPARED_EVENT));
			return;
		}
	}
	
	@Override
	public String toString(){
		return "DataPreparator" + " ";
	}
	
	protected void publishPreparedData(Data preparedData) {
		PLSOSMessageSystem.publish(new DataMessage(this, preparedData));
	}

}