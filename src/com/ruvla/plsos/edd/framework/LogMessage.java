package com.ruvla.plsos.edd.framework;

public class LogMessage extends PLSOSMessage {
	public final String message;
	
	public LogMessage(Object publisher, String message) {
		super(publisher, Subject.LOG);
		this.message = message;
	}
	
	@Override
	public String toString(){
		return super.toString() + "Message = \"" + message + "\" ";
	}
}
