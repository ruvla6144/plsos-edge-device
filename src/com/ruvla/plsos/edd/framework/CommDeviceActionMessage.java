package com.ruvla.plsos.edd.framework;

public class CommDeviceActionMessage extends PLSOSMessage {
	public enum CommDeviceAction {START, STOP}
	public final CommDeviceAction action;
	public final CommDeviceID commDeviceID;
	
	public CommDeviceActionMessage(Object publisher, CommDeviceAction action, CommDeviceID commDeviceID) {
		super(publisher, Subject.COMMDEVICE_ACTION);
		this.action = action;
		this.commDeviceID = commDeviceID;
	}
	
	@Override
	public String toString(){
		return super.toString() + "Action = " + action + " CommDevice ID = " + commDeviceID + " ";
	}
}
