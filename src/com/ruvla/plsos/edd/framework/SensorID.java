package com.ruvla.plsos.edd.framework;

public class SensorID{
	@Override
	public String toString() {
		return String.valueOf(id);
	}

	private final int id;
	
	public SensorID(int id){
		this.id = id;
	}
	
	@Override
	public int hashCode() {
		return (id + getClass().getName()).hashCode();
	}

	@Override
	public boolean equals(Object obj) {
		if (obj == null)
			return false;
		if (obj.getClass() == int.class) return id == (int)obj;
		if (this == obj)
			return true;
		if (getClass() != obj.getClass())
			return false;
		SensorID other = (SensorID) obj;
		if (id != other.id)
			return false;
		return true;
	}
	
	
}
