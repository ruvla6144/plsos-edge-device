package com.ruvla.plsos.edd.framework;

public class ConfirmationPacket {
	public final String string = "CONF";
	
	@Override
	public boolean equals(Object obj) {
		if (obj == null)
			return false;
		if (this == obj)
			return true;
		if (obj.getClass() == String.class) {
			return string == (String)obj;
		}
		if (getClass() != obj.getClass())
			return false;
		ConfirmationPacket other = (ConfirmationPacket) obj;
		if (string != other.string)
			return false;
		return true;
	}
}
