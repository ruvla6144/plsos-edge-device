package com.ruvla.plsos.edd.framework;

public class NewConnectorMessage extends PLSOSMessage {
	
	public final PLSOSConnector plsosConnector;

	public NewConnectorMessage(CommDevice publisher, PLSOSConnector connector) {
		super(publisher, Subject.COMMDEVICE_NEW_CONNECTOR);
		this.plsosConnector = connector;
	}
	
	public NewConnectorMessage(CommDeviceManager publisher, PLSOSConnector connector) {
		super(publisher, Subject.AGGREGATOR_DEVICE_DETECTED);
		this.plsosConnector = connector;
	}

}
