package com.ruvla.plsos.edd.framework;

public class PLSOSMessage {
	public enum Subject {
		
		ANY(null),
			SHUTDOWN_PLSOS(ANY),
			LOG(ANY),
			DATA(ANY),
				DATA_BATCH(DATA),
					DATA_FROM_STORAGE(DATA_BATCH),
				DATA_FROM_SENSOR(DATA),
				DATA_FROM_JOB(DATA),
				DATA_FROM_PREPARATOR(DATA),
			AGGREGATOR_DEVICE_DETECTED(ANY),
			AGGREGATOR_DEVICE_CONNECTED(ANY),
			AGGREGATOR_DEVICE_DISCONNECTED_EVENT(ANY),
			RETRIEVE_DATA_FROM_STORAGE(ANY),
			DATA_STORED_EVENT(ANY),
			DATA_PREPARED_EVENT(ANY),
			JOB_STOPPED_EVENT(ANY),
			COMMDEVICE(ANY), 
				COMMDEVICE_STARTED_EVENT(COMMDEVICE),
				COMMDEVICE_STOPPED_EVENT(COMMDEVICE),
				COMMDEVICE_NEW_CONNECTOR(COMMDEVICE),
				COMMDEVICE_DATA(COMMDEVICE),
			SCHEDULER(ANY),
				SENSOR_JOB_ACTION(SCHEDULER),
				START_DATA_PREPARATION(SCHEDULER),
				START_DATA_STORING(SCHEDULER),
				//START_DATA_TRANSFER(SCHEDULER),
				COMMDEVICE_ACTION(SCHEDULER),
			ERROR(ANY), //TODO
				SENSOR_ERROR(ERROR),
				AGG_ERROR(ERROR);
		
		private Subject parent;

		private Subject(Subject parent) {
	        this.parent = parent;
	    }
		
		public boolean is(Subject other) {
		    if (other == null) {
		        return false;
		    }
		   
		    for (Subject t = this;  t != null;  t = t.parent) {
		        if (other == t) {
		            return true;
		        }
		    }
		    return false;
		}
		
	};
	
	public final Subject subject;
	public final Object publisher;
	
	public PLSOSMessage(Object publisher, Subject subject){
		this.publisher = publisher;
		this.subject = subject;
	}
	
	@Override
	public String toString(){
		return "PLSOSMessage Subject = " + subject + " Publisher = " + publisher + " ";
	}
}