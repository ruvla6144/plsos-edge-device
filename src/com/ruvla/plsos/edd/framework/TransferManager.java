package com.ruvla.plsos.edd.framework;

import java.util.LinkedList;
import java.util.Queue;
import com.ruvla.plsos.edd.framework.PLSOSMessage.Subject;

public class TransferManager implements PLSOSMessageSubscriber {
	
	protected TransferSession transferSession;
	protected Thread transferSessionThread;
	protected Queue<DataBatch> batches = new LinkedList<DataBatch>();
	protected DataConverter dataConverter = Core.getFactory().createDataConverter();
	
	{
		PLSOSMessageSystem.subscribe(this, Subject.SHUTDOWN_PLSOS);
		PLSOSMessageSystem.subscribe(this, Subject.AGGREGATOR_DEVICE_DETECTED);
		PLSOSMessageSystem.subscribe(this, Subject.AGGREGATOR_DEVICE_DISCONNECTED_EVENT);
		PLSOSMessageSystem.subscribe(this, Subject.DATA_FROM_STORAGE);
	}
	
	protected void startTransfer(PLSOSConnector connector) {
		PLSOSMessageSystem.publish(new PLSOSMessage(this, Subject.RETRIEVE_DATA_FROM_STORAGE));
		transferSession = Core.getFactory().createTransferSession(connector, batches);
		transferSessionThread = new Thread(transferSession);
		transferSessionThread.start();
	}
	
	protected void stopTransfer() {
		if (transferSessionThread == null) return;
		transferSession.stop();
		transferSessionThread.interrupt();
		transferSessionThread = null;
	}
	
	protected void enqueue(DataBatch batch){
		synchronized (batches) {
			batches.add(batch);
			batches.notifyAll();
		}
	}
	
	protected void shutdown() {
		stopTransfer();
	}

	@Override
	public void handleMessage(PLSOSMessage message) throws PLSOSException {
		if (message.subject == Subject.AGGREGATOR_DEVICE_DETECTED) {
			startTransfer(((NewConnectorMessage) message).plsosConnector);
		} else if (message.subject == Subject.DATA_FROM_STORAGE) {
			enqueue(((DataBatchMessage) message).data);
		} else if (message.subject == Subject.AGGREGATOR_DEVICE_DISCONNECTED_EVENT) {
			stopTransfer();
		} else if (message.subject == Subject.SHUTDOWN_PLSOS){
			shutdown();
		}
		
	}
}
