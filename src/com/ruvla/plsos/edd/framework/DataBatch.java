package com.ruvla.plsos.edd.framework;

import java.util.ArrayList;
import java.util.List;

public class DataBatch {
	
	public static final int MAX_LENGTH = 16;
	
	protected List<Data> data;
	
	public DataBatch(List<Data> data) throws PLSOSException{
		if (data.size() <= MAX_LENGTH) this.data = data;
		else throw new PLSOSException("Too long batch");
	}
	
	public DataBatch() {
		data = new ArrayList<Data>();
	}
	
	public void addData(Data data) {
		this.data.add(data);
	}
	
	public List<Data> getDataList() {
		return data;
	}
	
	/*public byte[] toByteArray(){
		JSONObject jsonObject = new JSONObject();
		for (Data data : data){
			try {
				jsonObject.accumulate("Data", data.toJSON());
				
			} catch (JSONException e) {
				e.printStackTrace(); //TODO
			}
		}
		return jsonObject.toString().getBytes();
	}*/
}
