package com.ruvla.plsos.edd.framework;

import java.util.Timer;
import java.util.TimerTask;

import com.ruvla.plsos.edd.framework.CommDeviceActionMessage.CommDeviceAction;
import com.ruvla.plsos.edd.framework.JobActionMessage.JobAction;
import com.ruvla.plsos.edd.framework.PLSOSMessage.Subject;

public class Scheduler implements PLSOSMessageSubscriber {
	
	protected final long dataPreparationDelay = 1000 * 60 * 1 / 10;
	protected final long dataStoringDelay = 1000 * 60 * 1 / 10 + 2000;
	protected final int dataStorageThreadPriority = Thread.MIN_PRIORITY;
	protected final int dataPreparatorThreadPriority = Thread.MIN_PRIORITY;
	
	protected final Timer timer = new Timer();
	
	{
		PLSOSMessageSystem.subscribe(this, Subject.SHUTDOWN_PLSOS);
	}
	
	public Scheduler(){
		scheduleJobs();
		scheduleDataPreparator();
		scheduleDataStorage();
		scheduleCommDeviceManager();
	}
	
	protected void scheduleJobs() {
		for (JobID jobID : Core.getFactory().createJobIDList()){
			startJob(jobID);
		}
	}
	
	protected void scheduleDataPreparator() {
		TimerTask task = new TimerTask() {
			
			@Override
			public void run() {
				Thread.currentThread().setPriority(dataPreparatorThreadPriority);
				startDataPreparation();
			}
		};
		timer.scheduleAtFixedRate(task, dataPreparationDelay, dataPreparationDelay);
	}
	
	protected void scheduleDataStorage() {
		TimerTask task = new TimerTask() {
			
			@Override
			public void run() {
				Thread.currentThread().setPriority(dataStorageThreadPriority);
				startDataStoring();
			}
		};
		timer.scheduleAtFixedRate(task, dataStoringDelay, dataStoringDelay);
	}
	
	protected void scheduleCommDeviceManager() {
		for (CommDeviceID commDeviceID : Core.getFactory().createCommDeviceIDList()){
			startCommDevice(commDeviceID);
		}
	}
	
	protected void startDataPreparation() {
		PLSOSMessageSystem.publish(new PLSOSMessage(this, Subject.START_DATA_PREPARATION));
	}
	
	protected void startDataStoring() {
		PLSOSMessageSystem.publish(new PLSOSMessage(this, Subject.START_DATA_STORING));
	}
	
	protected void startCommDevice(CommDeviceID commDeviceID) {
		PLSOSMessageSystem.publish(new CommDeviceActionMessage(this, CommDeviceAction.START, commDeviceID));
	}
	
	protected void stopCommDevice(CommDeviceID commDeviceID) {
		PLSOSMessageSystem.publish(new CommDeviceActionMessage(this, CommDeviceAction.STOP, commDeviceID));
	}
	
	protected void startJob(JobID jobID) {
		PLSOSMessageSystem.publish(new JobActionMessage(this, JobAction.START, jobID));
	}
	
	protected void stopJob(JobID jobID) {
		PLSOSMessageSystem.publish(new JobActionMessage(this, JobAction.STOP, jobID));
	}
	
	protected void shutdown(){
		timer.cancel();
	}
	
	@Override
	public String toString(){
		return "Scheduler" + " ";
	}

	@Override
	public void handleMessage(PLSOSMessage message) throws PLSOSException {
		if (message.subject == Subject.SHUTDOWN_PLSOS) shutdown();
	}
}
