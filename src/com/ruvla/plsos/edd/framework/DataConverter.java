package com.ruvla.plsos.edd.framework;

public interface DataConverter {
	
	public byte[] convertDataToBytes(Data data);
	
	public Data convertBytesToData(byte[] bytes);
	
	public byte[] convertDataBatchToBytes(DataBatch dataBatch);
	
	public DataBatch convertBytesToDataBatch(byte[] bytes);
	
	public ConfirmationPacket convertBytesToConfirmationPacket(byte[] bytes);
}
