package com.ruvla.plsos.edd.framework;

import java.util.HashMap;
import java.util.Map;

import com.ruvla.plsos.edd.framework.JobActionMessage.JobAction;
import com.ruvla.plsos.edd.framework.PLSOSMessage.Subject;

public class SensorJobExecutor implements PLSOSMessageSubscriber {
	
	protected final Map<JobID, Thread> jobs;
	
	{
		PLSOSMessageSystem.subscribe(this, Subject.SHUTDOWN_PLSOS);
		PLSOSMessageSystem.subscribe(this, Subject.SENSOR_JOB_ACTION);
	}
	
	public SensorJobExecutor(){
		this.jobs = new HashMap<JobID, Thread>();
	}
	
	public void handleMessage(JobActionMessage message) throws PLSOSException{
		if (message.action == JobAction.START) start(message.jobID);
		else stop(message.jobID);
	}
	
	@Override
	public void handleMessage(PLSOSMessage message) throws PLSOSException {
		if (message instanceof JobActionMessage) {
			handleMessage((JobActionMessage)message);
		} else if (message.subject == Subject.SHUTDOWN_PLSOS){
			shutdown();
		}
	}
	
	protected void shutdown() {
		for (JobID jobID : jobs.keySet()){
			stop(jobID);
		}
	}
	
	private synchronized void stop(JobID jobID) {
		if (jobs.containsKey(jobID))
		{
			Thread job = jobs.get(jobID);
			while (!job.isInterrupted() && job.isAlive())
			{
				job.interrupt(); //TODO it is not enough to be sure that thread stopped
			}
			jobs.remove(jobID);
		}
		//TODO send error message
	}
	
	private synchronized void start(JobID jobID) throws PLSOSException{
		Thread thread = new Thread(Core.getFactory().createSensorJob(jobID));
		thread.start();
		jobs.put(jobID, thread);
	}
	
}