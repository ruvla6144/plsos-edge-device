package com.ruvla.plsos.edd.jmee.prototype;

import com.ruvla.plsos.edd.framework.PLSOSException;
import com.ruvla.plsos.edd.framework.Sensor;
import com.ruvla.plsos.edd.framework.Data;
import com.ruvla.plsos.edd.framework.SensorID;

public class SensorTest2 extends Sensor<CustomData2> {
	
	private volatile boolean needed;

	public SensorTest2(SensorID id) {
		super(id);
	}

	@Override
	public CustomData2 read() {
		return new CustomData2("GER!");
	}

	@Override
	public void start() throws InterruptedException, PLSOSException {
		needed = true;
		while(needed){
			Thread.sleep(1000);
			notifySubscribers(new CustomData2("GER! dataEvent"));
		}
	}

	@Override
	public void stop() {
		needed = false;
	}

	
}

class CustomData2 extends Data{
	public String her;
	public CustomData2(String str){
		her = str;
	}
	@Override
	public String toString() {
		return "CustomData2 her = \"" + her + "\" ";
	}
}