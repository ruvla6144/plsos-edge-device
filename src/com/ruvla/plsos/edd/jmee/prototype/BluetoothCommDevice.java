package com.ruvla.plsos.edd.jmee.prototype;

import java.io.IOException;
import java.nio.ByteBuffer;

import com.ruvla.plsos.edd.framework.AggregatorDeviceID;
import com.ruvla.plsos.edd.framework.CommDevice;
import com.ruvla.plsos.edd.framework.CommDeviceID;
import com.ruvla.plsos.edd.framework.Core;
import com.ruvla.plsos.edd.framework.LogMessage;
import com.ruvla.plsos.edd.framework.PLSOSConnector;
import com.ruvla.plsos.edd.framework.PLSOSMessageSystem;

import jdk.dio.DeviceManager;
import jdk.dio.uart.UART;
import jdk.dio.uart.UARTConfig;
import jdk.dio.uart.UARTEvent;
import jdk.dio.uart.UARTEventListener;

public class BluetoothCommDevice extends CommDevice implements UARTEventListener {
	
	private UARTConfig config = null;
	private UART uart = null;
	private String controllerName = "ttyAMA0";
	private int baudRate = 115200;
	private volatile boolean stopped = false;
	private int maxStringSize = 16;

	public BluetoothCommDevice(CommDeviceID commDeviceID) {
		super(commDeviceID);
	}

	@Override
	public void startDevice() throws InterruptedException {
		
		config = new UARTConfig.Builder()
				.setBaudRate(baudRate)
				.setControllerName(controllerName).build();
		
		ByteBuffer bufferOut;
		
		try {
			uart = DeviceManager.open(config);
			
			uart.setEventListener(UARTEvent.INPUT_DATA_AVAILABLE, this);
			
			byte[] array = "AT+RESET\r\n".getBytes();
			bufferOut = ByteBuffer.wrap(array);
			uart.write(bufferOut);
			
		} catch (IOException e) {
			e.printStackTrace(); // TODO 
		}
	}

	@Override
	public void stopDevice() {
		if (!stopped)
			try {
				uart.close();
			} catch (IOException e) { 
				e.printStackTrace(); // TODO
			}
		stopped = true;
	}

	@Override
	public void connect(PLSOSConnector connector) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void disconnect(PLSOSConnector connector) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void send(PLSOSConnector connector, byte[] data) {
		ByteBuffer buffer = ByteBuffer.wrap(data);
		try {
			Thread.sleep(100); //TODO
			synchronized (uart) {
				uart.write(buffer);
			}
		} catch (IOException | InterruptedException e) {
			e.printStackTrace(); // TODO
		}
	}

	@Override
	public byte[] receive(PLSOSConnector connector) {
		// TODO Auto-generated method stub
		return null;
	}
	
	private String getStringFromUART(int maxSize) {
		ByteBuffer buffer = ByteBuffer.allocateDirect(16);
		
		String inputData = null;
		
		try {
			uart.read(buffer);
			int pos = buffer.position();
			buffer.rewind();
			byte[] res = new byte[pos];
			buffer.get(res);
			inputData = new String(res);
			PLSOSMessageSystem.publish(new LogMessage(this, new String(res)));
			return inputData;
		} catch (IOException e) {
			e.printStackTrace(); // TODO
		}
		
		return null;
	}

	@Override
	public void eventDispatched(UARTEvent event) {
		
		String fromUART = getStringFromUART(maxStringSize);
		
		if (fromUART.contains("OK+CONN")) send(null, String.valueOf(Core.ID).getBytes());
		else if (fromUART.contains("OK+LOST")) onAggregatorLost();
		else {
			
			try {
				Long aggID = Long.valueOf(fromUART);
				publishNewConnector(new PLSOSConnector(commDeviceID, new AggregatorDeviceID(aggID.longValue())));
			}
			catch (NumberFormatException e) {
				publishNewData(fromUART.getBytes());
			}	
		}
		
	}

}
