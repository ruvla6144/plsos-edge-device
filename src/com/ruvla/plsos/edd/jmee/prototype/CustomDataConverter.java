package com.ruvla.plsos.edd.jmee.prototype;

import org.json.me.JSONException;
import org.json.me.JSONObject;

import com.ruvla.plsos.edd.framework.ConfirmationPacket;
import com.ruvla.plsos.edd.framework.Data;
import com.ruvla.plsos.edd.framework.DataBatch;
import com.ruvla.plsos.edd.framework.DataConverter;
import com.ruvla.plsos.edd.framework.LogMessage;
import com.ruvla.plsos.edd.framework.PLSOSMessageSystem;

public class CustomDataConverter implements DataConverter {

	@Override
	public byte[] convertDataToBytes(Data data) {
		if (data instanceof HCSR04Data)
			return ((HCSR04Data)data).toJSON().toString().getBytes();
		else {
			PLSOSMessageSystem.publish(new LogMessage(this, "Wrong data format"));
			return null;
		}
	}

	@Override
	public Data convertBytesToData(byte[] bytes) {
		try {
			return new HCSR04Data(Double.valueOf(new JSONObject(new String(bytes)).getJSONObject("HCSR04Data").getDouble("Distance")));
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}

	@Override
	public byte[] convertDataBatchToBytes(DataBatch dataBatch) {
		JSONObject jsonObject = new JSONObject();
		for (Data data : dataBatch.getDataList()){
			try {
				jsonObject.accumulate("Data", ((HCSR04Data)data).toJSON());
				
			} catch (JSONException e) {
				e.printStackTrace(); //TODO
				return null;
			}
		}
		return jsonObject.toString().getBytes();
	}

	@Override
	public DataBatch convertBytesToDataBatch(byte[] bytes) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public ConfirmationPacket convertBytesToConfirmationPacket(byte[] bytes) {
		// TODO Auto-generated method stub
		return new ConfirmationPacket();
	}

}
