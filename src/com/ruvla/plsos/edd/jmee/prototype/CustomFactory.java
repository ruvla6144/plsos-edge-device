package com.ruvla.plsos.edd.jmee.prototype;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.ruvla.plsos.edd.framework.CommDevice;
import com.ruvla.plsos.edd.framework.CommDeviceID;
import com.ruvla.plsos.edd.framework.DataConverter;
import com.ruvla.plsos.edd.framework.JobID;
import com.ruvla.plsos.edd.framework.Sensor;
import com.ruvla.plsos.edd.framework.SensorID;
import com.ruvla.plsos.edd.framework.SensorJob;
import com.ruvla.plsos.edd.jmee.defaults.DefaultFactory;

public class CustomFactory extends DefaultFactory{
	
	private final Map<SensorID, Sensor<?>> sensors = new HashMap<SensorID, Sensor<?>>();
	
	public CustomFactory(){
		super();
		sensors.put(new SensorID(256), new SensorTest2(new SensorID(256)));
		sensors.put(new SensorID(4), new HCSR04Sensor(new SensorID(4)));
	}

	@Override
	public SensorJob<?> createSensorJob(JobID id) {
		return new HCSR04Job(id);
	}

	@Override
	public Sensor<?> createSensor(SensorID id) {
		return sensors.get(id);
	}

	@Override
	public List<JobID> createJobIDList() {
		List<JobID> list = new ArrayList<JobID>();
		list.add(new JobID(32768));
		return list;
	}

	@Override
	public List<CommDeviceID> createCommDeviceIDList() {
		List<CommDeviceID> list = new ArrayList<CommDeviceID>();
		list.add(new CommDeviceID(768, "Bluetik"));
		return list;
	}

	@Override
	public CommDevice createCommDevice(CommDeviceID commDeviceID) {
		return new BluetoothCommDevice(commDeviceID);
	}

	@Override
	public DataConverter createDataConverter() {
		return new CustomDataConverter();
	}
	
}