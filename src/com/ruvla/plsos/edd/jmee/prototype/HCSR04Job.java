package com.ruvla.plsos.edd.jmee.prototype;

import com.ruvla.plsos.edd.framework.Core;
import com.ruvla.plsos.edd.framework.Data;
import com.ruvla.plsos.edd.framework.DataMessage;
import com.ruvla.plsos.edd.framework.JobID;
import com.ruvla.plsos.edd.framework.PLSOSException;
import com.ruvla.plsos.edd.framework.Sensor;
import com.ruvla.plsos.edd.framework.SensorID;
import com.ruvla.plsos.edd.framework.SensorJob;

public class HCSR04Job extends SensorJob<HCSR04Data> {
	
	Sensor<HCSR04Data> hcsr04Sensor;

	@SuppressWarnings("unchecked")
	public HCSR04Job(JobID id) {
		super(id);
		hcsr04Sensor = (Sensor<HCSR04Data>) Core.getFactory().createSensor(new SensorID(4));
	}

	@Override
	public void execute() throws InterruptedException, PLSOSException {
		hcsr04Sensor.start();
		while (true) {
			publishJobData(new HCSR04Data(hcsr04Sensor.read().distance));
			Thread.sleep(2000);
		}
	}

	@Override
	protected void finish() {
		try {
			hcsr04Sensor.stop();
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}

	@Override
	public void handleSensorData(Sensor<?> sensor, DataMessage sensorData) throws PLSOSException {
		
	}

}