package com.ruvla.plsos.edd.jmee.prototype;

import org.json.me.JSONException;
import org.json.me.JSONObject;

import com.ruvla.plsos.edd.framework.Data;
import com.ruvla.plsos.edd.framework.LogMessage;
import com.ruvla.plsos.edd.framework.PLSOSMessageSystem;

public class HCSR04Data extends Data {

	public final double distance;
	
	public HCSR04Data(double distance) {
		this.distance = distance;
	}
	
	@Override
	public String toString() {
		return "HCSR04Data " + distance;
	}
	
	public JSONObject toJSON() {
		JSONObject jsonObjectO = new JSONObject();
		JSONObject jsonObjectI = new JSONObject();
		try {
			jsonObjectO.put("HCSR04Data", jsonObjectI);
			jsonObjectI.put("Distance", distance);
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return jsonObjectO;
	}

}
