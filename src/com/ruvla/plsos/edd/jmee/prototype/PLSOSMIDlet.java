package com.ruvla.plsos.edd.jmee.prototype;

import javax.microedition.midlet.MIDlet;
import javax.microedition.midlet.MIDletStateChangeException;

import com.ruvla.plsos.edd.framework.AbstractFactory;
import com.ruvla.plsos.edd.framework.Core;
import com.ruvla.plsos.edd.framework.PLSOSException;

public class PLSOSMIDlet extends MIDlet {
	
	Core core;

	public PLSOSMIDlet() {
		
	}

	@Override
	protected void destroyApp(boolean unconditional) throws MIDletStateChangeException {
		core.shutdown();
	}

	@Override
	protected void startApp() throws MIDletStateChangeException {

		AbstractFactory customFactory = new CustomFactory();
		
		try {
			core = new Core(customFactory);
		} catch (PLSOSException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

}