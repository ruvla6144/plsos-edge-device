package com.ruvla.plsos.edd.jmee.prototype;

import java.io.IOException;

import com.ruvla.plsos.edd.framework.LogMessage;
import com.ruvla.plsos.edd.framework.PLSOSMessageSystem;
import com.ruvla.plsos.edd.framework.PLSOSException;
import com.ruvla.plsos.edd.framework.Sensor;
import com.ruvla.plsos.edd.framework.Data;
import com.ruvla.plsos.edd.framework.SensorID;

import jdk.dio.DeviceManager;
import jdk.dio.gpio.GPIOPin;
import jdk.dio.gpio.GPIOPinConfig;

public class HCSR04Sensor extends Sensor<HCSR04Data> {
	
	private final int TRIG_PIN = 24;
	private final int ECHO_PIN = 23;
	
	private final int PULSE = 10000;        // #10 �s pulse = 10,000 ns
    private final int SPEEDOFSOUND = 34029; // Speed of sound = 34029 cm/s

    private GPIOPin trigger = null;
    private GPIOPin echo = null;
	
	public HCSR04Sensor(SensorID id) {
		super(id);
	}
	
	public double pulse() throws InterruptedException {
	  long distance = 0;
	  try {
	      trigger.setValue(true); //Send a pulse trigger; must be 1 and 0 with a 10 �s wait
	      Thread.sleep(0, PULSE);
	      trigger.setValue(false);
	      long starttime = System.nanoTime(); //ns
	      long stop = starttime;
	      long start = starttime;
	      //echo will go 0 to 1 and need to save time for that. 2 seconds difference
	      while ((!echo.getValue()) && (start < starttime + 1000000000L * 2)) {
	          start = System.nanoTime();
	      }
	      while ((echo.getValue()) && (stop < starttime + 1000000000L * 2)) {
	          stop = System.nanoTime();
	      }
	      long delta = (stop - start);
	      distance = delta * SPEEDOFSOUND; // echo from 0 to 1 depending on object distance
	  } catch (IOException ex) {
	      PLSOSMessageSystem.publish(new LogMessage(this, "HCSR04 : " + ex));
	  }
	  return distance / 2.0 / (1000000000L); // cm/s
	}
	
	public void close() {
	    if ((trigger!=null) && (echo!=null)){
	    	try {
				trigger.close();
				echo.close();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
	    }
	}
    
	@Override
	public HCSR04Data read() throws InterruptedException {
		return new HCSR04Data(pulse());
	}
	
	@Override
	public void start() throws InterruptedException, PLSOSException {
		try {
			trigger = (GPIOPin) DeviceManager.open(new GPIOPinConfig.Builder().
					setControllerNumber(0).
					setPinNumber(TRIG_PIN).
					setDirection(GPIOPinConfig.DIR_OUTPUT_ONLY).
					setDriveMode(GPIOPinConfig.MODE_OUTPUT_PUSH_PULL).
					setTrigger(GPIOPinConfig.TRIGGER_NONE).
					setInitValue(false).
					build());
			echo = (GPIOPin) DeviceManager.open(new GPIOPinConfig.Builder().
					setControllerNumber(0).
					setPinNumber(ECHO_PIN).
					setDirection(GPIOPinConfig.DIR_INPUT_ONLY).
					setDriveMode(GPIOPinConfig.MODE_INPUT_PULL_UP).
					setTrigger(GPIOPinConfig.TRIGGER_NONE).
					setInitValue(false).
					build());
			Thread.sleep(500);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}		
	}
	
	@Override
	public void stop() throws InterruptedException {
		close();
	}
}