package com.ruvla.plsos.edd.jmee.defaults;

import java.util.Queue;

import com.ruvla.plsos.edd.defaults.DefaultDataPreparator;
import com.ruvla.plsos.edd.defaults.DefaultLogger;
import com.ruvla.plsos.edd.defaults.DefaultTransferSession;
import com.ruvla.plsos.edd.framework.AbstractFactory;
import com.ruvla.plsos.edd.framework.CommDeviceManager;
import com.ruvla.plsos.edd.framework.DataBatch;
import com.ruvla.plsos.edd.framework.DataPreparator;
import com.ruvla.plsos.edd.framework.DataStorage;
import com.ruvla.plsos.edd.framework.Logger;
import com.ruvla.plsos.edd.framework.PLSOSConnector;
import com.ruvla.plsos.edd.framework.Scheduler;
import com.ruvla.plsos.edd.framework.SensorJobExecutor;
import com.ruvla.plsos.edd.framework.TransferManager;
import com.ruvla.plsos.edd.framework.TransferSession;

public abstract class DefaultFactory extends AbstractFactory {
	
	@Override
	public Logger createLogger() {
		return new DefaultLogger();
	}
	
	@Override
	public SensorJobExecutor createSensorJobExecutor(){
		return new SensorJobExecutor();
	}
	
	@Override
	public Scheduler createScheduler(){
		return new Scheduler();
	}
	
	@Override
	public CommDeviceManager createCommDeviceManager(){
		return new CommDeviceManager();
	}
	
	@Override
	public TransferManager createTransferManager() {
		return new TransferManager();
	}

	@Override
	public DataPreparator createDataPreparator() {
		// TODO Auto-generated method stub
		return new DefaultDataPreparator();
	}

	@Override
	public DataStorage createDataStorage() {
		// TODO Auto-generated method stub
		return new DefaultDataStorage();
	}
	
	@Override
	public TransferSession createTransferSession(PLSOSConnector connector, Queue<DataBatch> batches) {
		return new DefaultTransferSession(connector, batches);
	}
}
