package com.ruvla.plsos.edd.jmee.defaults;

import javax.microedition.rms.RecordEnumeration;
import javax.microedition.rms.RecordStore;
import javax.microedition.rms.RecordStoreException;
import javax.microedition.rms.RecordStoreNotOpenException;

import com.ruvla.plsos.edd.framework.Core;
import com.ruvla.plsos.edd.framework.Data;
import com.ruvla.plsos.edd.framework.DataBatch;
import com.ruvla.plsos.edd.framework.DataConverter;
import com.ruvla.plsos.edd.framework.DataStorage;
import com.ruvla.plsos.edd.framework.LogMessage;
import com.ruvla.plsos.edd.framework.PLSOSMessageSystem;


public class DefaultDataStorage extends DataStorage {
	protected RecordStore store;
	protected DataConverter dataConverter = Core.getFactory().createDataConverter(); 

	{
		try {
			store = RecordStore.openRecordStore("PLSOSDataStorage", true, RecordStore.AUTHMODE_PRIVATE, true);
			PLSOSMessageSystem.publish(new LogMessage(this, String.valueOf("STORE HAS " + store.getNumRecords()) + " RECORDS"));
		} catch (RecordStoreException e) {
			e.printStackTrace(); //TODO
		}
	}

	@Override
	public void storeData() {
		try{
			while (!preparedData.isEmpty()){
				byte[] bytes;
				synchronized (preparedData) {
					bytes = dataConverter.convertDataToBytes(preparedData.poll());
				}
				store.addRecord(bytes, 0, bytes.length);
			}
			PLSOSMessageSystem.publish(new LogMessage(this, "STORE HAS " + store.getNumRecords() + " RECORDS"));
		}
		catch (RecordStoreException e){
			e.printStackTrace(); //TODO
		}
	}

	@Override
	public Data retrieveData() {
		RecordEnumeration enumer;
		Data data = null;
		try {
			enumer = store.enumerateRecords(null, null, false);
			if (enumer.hasNextElement()) {
				data = dataConverter.convertBytesToData((enumer.nextRecord()));
				//store.deleteRecord(enumer.previousRecordId());
			}
		} catch (RecordStoreException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return data;
	}

	@Override
	protected void shutdown() {
		store.close();
	}

	@Override
	public boolean hasData() {
		try {
			return store.getNumRecords() != 0;
		} catch (RecordStoreNotOpenException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return false;
	}

	@Override
	public DataBatch retrieveDataBatch() {
		RecordEnumeration enumer = null;
		DataBatch batch = new DataBatch();
		try {
			enumer = store.enumerateRecords(null, null, false);
			for (int i = 0; (i < DataBatch.MAX_LENGTH) && enumer.hasNextElement(); i++) {
				batch.addData(dataConverter.convertBytesToData((enumer.nextRecord())));
				store.deleteRecord(enumer.getRecordId(i));
			}
		} catch (RecordStoreException e) {
			e.printStackTrace(); //TODO
		}
		enumer.destroy();
		return batch;
	}
}
